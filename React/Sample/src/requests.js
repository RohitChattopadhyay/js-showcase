


export function getUsers() {
  return delay(1000) // To allow the video demo to clearly show the loading message
    .then(() => fetch('https://jsonplaceholder.typicode.com/users'))
    .then(response => response.json())
}

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}