import React from 'react'
import ReactDOM from 'react-dom'
import cx from 'classname'

class Popover extends React.Component {

  constructor() {

    this.state = {
      open: false,
      style: { top: 0, left: 0 },
    }

    this.triggerRef = React.createRef()
    this.popoverRef = React.createRef()

    /* optimization: append child on first time component is opened */
    this.mountNode = document.createElement('div')
    document.body.appendChild(this.mountNode)

  }

  componentWillUnmount() {
    document.body.removeChild(this.mountNode)
  }

  componentDidUpdate() {
    this.updatePosition()
  }

  updatePosition() {
    const trigger = this.triggerRef.current.getBoundingClientRect()
    const popover = this.popoverRef.current.getBoundingClientRect()

    const top = trigger.top
    const left = trigger.left + trigger.width + 10

    const {style} = this.state

    if (top !== style.top || left !== style.left) {
      this.setState({ style: { top, left } })
    }
  }

  open() {
    this.setState({ open: true })
  }

  close() {
    this.setState({ open: false })
  }

  render() {
    const {children, content} = this.props
    const {open, style} = this.state

    /* replace trigger with a clone that catches onMouseOver/Out events */
    const trigger = React.cloneElement(
      children,
      {
        ref: this.triggerRef,
        onMouseOver: () => this.open(),
        onMouseOut: () => this.close(),
      },
      asArray(children.props.children).concat(
        ReactDOM.createPortal(
          <div 
            className={cx('Popover', { open })}
            style={style}
            ref={this.popoverRef}
          >
            { content }
          </div>
        , this.mountNode)
      )
    )

    return (
      trigger
    )
  }
}

function asArray(value) {
  return Array.isArray(value) ? value : [value]
}

export default Popover;