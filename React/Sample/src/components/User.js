import React from 'react';

import Popover from './Popover.js'

function User({ data }) {

  const popoverContent = (
    <div>
      <div><strong>Street</strong> {data.address.street}</div>
      <div><strong>Suite</strong> {data.address.suite}</div>
      <div><strong>City</strong> {data.address.city}</div>
      <div><strong>Zip</strong> {data.address.zipcode}</div>
    </div>
  )

  return (
    <div className='User'>
      <Popover content={popoverContent}>
        <span className='User__name'>{data.name}</span>
      </Popover>
      <div className='User__fill' />
      <span className='text-muted'>({data.username})</span>
    </div>
  )
}

export default User;