import React, { Component } from 'react';
import { render } from 'react-dom';
import User from './components/User';
import './style.css';

import { getUsers } from './requests.js'

class App extends Component {
  
  state = {
    isLoading: true,
    users: [],
  }

  componentDidMount() {
    getUsers()
    .then(users => {
      this.setState({ isLoading: false, users })
    })
  }

  render() {
    const {isLoading, users} = this.state

    return (
      <div>
        <h1>Users { isLoading && '(loading)' }</h1>

        <div className='userList'>
          {
            users.map(user =>
              <User data={user} />
            )
          }
        </div>
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
