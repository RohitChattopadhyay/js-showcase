import React, { Component } from "react";

import '../styles/AnswerComponent.css';
import Comment from '../components/Comment'

class AnswerComponent extends Component {
    render() {
        return (
          <div className={`row answerSection`}>
          {/* {console.log(this.props.content)} */}
             <div className={`answerBody`}>
             {this.props.content.body}
             </div>
             <div className={`answerDetails w-100`}>
              <span>
                <small>answered Nov 7 '08 at 19:06</small><br />
                Rohit Chattopadhyay<br/>
              </span>
             </div>
              <Comment content={this.props.content.comment} />
          </div>
        );
    }
}

export default AnswerComponent;