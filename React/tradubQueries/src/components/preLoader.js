import React, { Component } from "react";

import '../styles/preLoader.css';
class PreLoader extends Component {
    render() {
        return (
            <div className={` h-100 w-100 preloader`}>
            <div className={`row h-100 w-100 justify-content-center align-items-center`}>
                <p>{this.props.msg}</p>
            </div>
          </div>
        );
    }
}

export default PreLoader;