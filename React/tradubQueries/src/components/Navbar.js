import React, { Component } from "react";
import { NavLink } from 'react-router-dom'

import '../styles/Navbar.css';

class Navbar extends Component {
    render() {
        return (
            <nav className={`navbar navbar-expand-lg navbar-light bg-light`}>
                <NavLink activeClassName="active" to="./" className={`navbar-brand`}>Tradub Queries</NavLink>
                <button className={`navbar-toggler`} type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className={`navbar-toggler-icon`}></span>
                </button>

                <div className={`collapse navbar-collapse`} id="navbarSupportedContent">

                <form className={`form-inline my-2 my-lg-0 col`}>
                        <input className={`form-control mr-sm-2 col roundedBorder`} type="search" placeholder="Search" aria-label="Search" />
                        <button className={`btn btn-outline-tradub my-2 my-sm-0 roundedBorder`} type="submit">Add Query</button>
                    </form>
                    <ul className={`navbar-nav mr-auto`}>
                    <li className={`nav-item`}>
                        <NavLink activeClassName="active" exact className={`nav-link`} to="/queries/"><i className={`fas fa-home`}></i> Home <span className={`sr-only`}>(current)</span></NavLink>
                    </li>
                    <li className={`nav-item`}>
                        <NavLink activeClassName="active" to="/queries/posts/12345678" className={`nav-link `} href="#"><i className={`fas fa-pen-alt`}></i> Answer<span className={`sr-only`}>(current)</span></NavLink>
                    </li>
                    <li className={`nav-item`}>
                        <a className={`nav-link`} href="#"><i className={`far fa-bell`}></i> Notification <span className={`sr-only`}>(current)</span></a>
                    </li>

                    <li className={`nav-item dropdown`}>
                        <a className={`nav-link dropdown-toggle`} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hi {this.props.firstName}
                        </a>
                        <div className={`dropdown-menu`} aria-labelledby="navbarDropdown">
                        <a className={`dropdown-item`} href="#">Profile</a>
                        <a className={`dropdown-item`} href="#">Settings</a>
                        <div className={`dropdown-divider`}></div>
                        <a className={`dropdown-item`} href="#">Policies</a>
                        </div>
                    </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navbar;