import React, { Component } from "react";

import '../styles/Comment.css';

class Comment extends Component {
    constructor(){
        super()
        this.state = {
            json : [],
            anonymous : false,
            comment : ""
        }
        this.addComment = this.addComment.bind(this);
        this.setComment = this.setComment.bind(this);
        this.changeAnonymous = this.changeAnonymous.bind(this);
    }
    componentDidMount(){
        this.setState({
            json : this.props.content
        })
        console.log(this.props.content)
    }
    addComment(event){
        event.preventDefault();
        //Add API
        let tempJSON = this.state.json
        if(!this.state.anonymous)
            tempJSON.unshift({
                content : this.state.comment,
                userName: 'Rohit Comments',
                userId : 2425
            })
        else
            tempJSON.unshift({
                content : this.state.comment,
                userName: 'Anonymous',
                userId : false
            })
        this.setState(() => ({
            json: tempJSON
        }))
        event.target.reset()
    }
    changeAnonymous(event){
        event.persist();
        this.setState(() => ({
            anonymous: event.target.checked
        }))
    }
    setComment(event){
        this.setState({
            comment: event.target.value
        })
        //slow
    }

    render() {
        if(!this.props.content)
            return(          <div className={`container commentSection`}>
            <div className={`w-100`}>                
                <form onSubmit={this.addComment} className={`w-100`}>
                    <div className={`input-group mb-3`}>
                        <input type="text" className={`form-control`} placeholder="Add a comment" onKeyUp={this.setComment} />
                        <div className={`input-group-append`}>
                            <button className={`btn btn-outline-secondary`} type="submit">Comment</button>
                        </div>
                    </div>
                    <div className={`input-group mb-3 anonymousBlock`}>
                        <small className={`w-100`}>
                            <label>
                            Anonymous &nbsp;
                            <input
                                name="anonComment"
                                type="checkbox"
                                checked={this.state.anonymous}
                                onChange={this.changeAnonymous} />
                            </label>
                        </small>
                    </div>
                </form>
            </div>
          </div>)
        return (
          <div className={`container commentSection`}>
            <div className={`w-100`}>                
                <form onSubmit={this.addComment} className={`w-100`}>
                    <div className={`input-group mb-3`}>
                        <input type="text" className={`form-control`} placeholder="Add a comment" onKeyUp={this.setComment} />
                        <div className={`input-group-append`}>
                            <button className={`btn btn-outline-secondary`} type="submit">Comment</button>
                        </div>
                    </div>
                    <div className={`input-group mb-3 anonymousBlock`}>
                        <small className={`w-100`}>
                            <label>
                            Anonymous &nbsp;
                            <input
                                name="anonComment"
                                type="checkbox"
                                checked={this.state.anonymous}
                                onChange={this.changeAnonymous} />
                            </label>
                        </small>
                    </div>
                </form>
            </div>
            <div className={`w-100`}>
                <ul>
                    {this.state.json.map(
                        comment=> {
                            return (
                                <li key={comment.userName /*needs to be changed to comment id*/}>
                                    <div className={`w-100`}>
                                        <p>
                                            <b>{comment.userName}</b><br /><small>{comment.timestamp}</small><br />{comment.content}
                                        </p>
                                    </div>
                                </li>
                            )
                        }
                    )}
                </ul>
            </div>
          </div>
        );
    }
}

export default Comment;