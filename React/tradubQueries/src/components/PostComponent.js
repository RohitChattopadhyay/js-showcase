import React, { Component } from "react";

import AnswerComponent from '../components/AnswerComponent'
import Comment from '../components/Comment'
import PreLoader from '../components/preLoader'

import '../styles/PostComponent.css';
const API  = 'https://raw.githubusercontent.com/RohitChattopadhyay/tradub-queries/master/jsonTemplate/';
const Token = 'AS9NtPzjuE9Y26cq0ZqHXPk_h9yOK2z0ks5cT9GIwA'
class PostComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      loading: true,
      ansPage: 0,
      maxPage: 0,
      loaderMsg: "Loading...",
      jsonContent : {}
    };
    this.pagination = this.pagination.bind(this);
    this.pageChange = this.pageChange.bind(this);
  }
  componentWillMount() {
    fetch(API + 'post.json' + '?token=' + Token)
        .then(response => response.json())
        .then(data => {
            this.setState({
                jsonContent: data,
                loading: false,
                maxPage: data.answer.length-1
            })
        })
        .catch(()=>{
          this.setState({
            loaderMsg: "Please try again later. If you see this error frequently contact us."
          })
        })
  }
  pagination(){
    let content = [];
    let i;
    for(i=0;i<=this.state.maxPage;i++){
      if(i==0){
        if(this.state.ansPage>i){
          content.push(
            <li class="page-item">
              <a class="page-link" aria-label="First"  onClick={this.pageChange} data-topage= {0} > 
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">First</span>
              </a>
            </li>            
            )
            content.push(<li class="page-item"><a class="page-link"  onClick={this.pageChange} data-topage= {this.state.maxPage-1}onClick={this.pageChange} data-topage= {i} >{i+1}</a></li>)
        }
        else 
          content.push(<li class="page-item active"><a class="page-link" >{i+1}</a></li>)
      }
      else if(i==this.state.maxPage){
        if(this.state.ansPage==i){   
          content.push(<li class="page-item active"><a class="page-link" >{i+1}</a></li>)
        }
        else{
          content.push(<li class="page-item"><a class="page-link"  onClick={this.pageChange} data-topage= {this.state.maxPage} >{i+1}</a></li>)
          content.push(
            <li class="page-item">
              <a class="page-link" onClick={this.pageChange} data-topage= {this.state.maxPage} aria-label="Last">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Last</span>
              </a>
            </li>
            )
        }
      }
      else{
        if(this.state.ansPage==i){
          content.push(<li class="page-item active"><a class="page-link">{i+1}</a></li>);
        }
        else{
          content.push(<li class="page-item"><a onClick={this.pageChange} data-topage= {i} class="page-link">{i+1}</a></li>);
        }
      }
    }
    return content;
  }
  pageChange(event){
    this.setState({
      ansPage : event.target.dataset.topage
    })
  }

    render() {
      console.log(this.props.match.params.id)
      if(this.state.loading) return (<PreLoader msg={this.state.loaderMsg}/>)
      else
        return (
          <div className={`row`}>
            <div className={`col-sm-8`} >
              <div className={`w-100`}>
                  <div>
                    <a href="#" className={`badge badge-secondary`}>Tag1</a>
                    <a href="#" className={`badge badge-secondary`}>Tag2</a>
                  </div>
                  <span className={`w-100 queText`} >
                    {this.state.jsonContent.question}
                  </span>
                  <div className={`col-12 queOptions`}>
                  <span className={`queOptionsLeft`}>
                    <i className={`fas fa-pen-alt`}></i> Answer
                  </span>
                  <span className={`queOptionsRight`}>
                    <i class="fab fa-facebook-f"></i>
                    <i class="fab fa-twitter"></i>
                  </span>
                  <hr /><div className={`row answerSection`}>
             <div className={`answerBody`}>
             {this.state.jsonContent.queBody}
             </div>
             <div className={`answerDetails w-100`}>
              <span>
                <small>asked Nov 7 '08 at 19:06</small><br />
                {this.state.jsonContent.user.name}<br/>
              </span>
             </div>
             <Comment content={this.state.jsonContent.comment}/>
          </div>
          <h5>{this.state.jsonContent.totalAnswer>1?(this.state.jsonContent.totalAnswer +" Answer"):(this.state.jsonContent.totalAnswer +" Answers")}</h5><hr />
                  <div className={`answerContainer`}>
                    {/* <AnswerComponent />                    
                    <AnswerComponent /> */}
                    {
                      
                    }
                    {(this.state.jsonContent.answer[this.state.ansPage])?(this.state.jsonContent.answer[this.state.ansPage].map(
                        answer=> {
                            return (
                              <AnswerComponent content={answer} />
                            )
                        }
                    )):window.location.reload()}
                  </div>
                  <div className={`w-100 pagination`}>
                    <nav aria-label="Page navigation" className={`w-100`}>
                      <ul class="pagination justify-content-end">
                      {
                        this.pagination()
                      }
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <div className={`col-sm-4`} >
            Show tips/ads here
            </div>
          </div>
        );
    }
}

export default PostComponent;