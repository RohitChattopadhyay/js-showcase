import React, { Component } from "react"
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import PostComponent from '../components/PostComponent'
import Home from '../components/Home'

import '../styles/App.css';

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Navbar firstName="Rohit"/>
                    <div className={`container app-body`}>                    
                        <Switch>
                            <Route exact path="/queries/" component={Home} />
                            <Route path="/queries/posts/:id" component={PostComponent} />
                            {/* <Route path="/contact" component={Contact} /> */}
                            {/* <Route component={Notfound} /> */}
                        </Switch>
                    </div>
                    <Footer />
                </div>
            </Router>                    
        );
    }
}

export default App; 
