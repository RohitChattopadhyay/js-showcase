import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
class CardDisplay extends React.Component {
    constructor(props) {
        super()
        this.state = {
            content : props.contentArr       
        }
    }
    render() {
        return (
            <View>
                {this.state.content.map(item => 
                        <Card key={item.id}  style={styles.card}>
                          <Card.Content>
                                <Title>
                                    <Text style={styles.id}>
                                        ({item.id})
                                    </Text>
                                    {item.name}
                                </Title>
                                <Paragraph>
                                    Age: {item.age}{"\n"}
                                    Purpose: {item.res}
                                </Paragraph>
                          </Card.Content>
                        </Card>
                    )}              
            </View>
        )
    }
}
export default CardDisplay


const styles = StyleSheet.create({
  id: {
    fontSize:17 
  },
  card: {
    borderBottomWidth: 1,
    borderTopWidth:1
  }
});