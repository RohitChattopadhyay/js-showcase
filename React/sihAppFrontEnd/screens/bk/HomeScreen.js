import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  state = {
    text: ''
  }
  render() {
    return (
      <View>
        <Card>
          <Card.Title title="My Patient" left={(props) => <Avatar.Icon {...props}  icon ="person-pin"/>} />
          <Card.Content>
          </Card.Content>
        </Card>
        <Card> 
          <Card.Title title="Appointment" left={(props) => <Avatar.Icon {...props} icon="date-range" />} />
          <Card.Content>
          </Card.Content>
        </Card>
        <Card>
              <Card.Title title="Search" left={(props) => <Avatar.Icon {...props} icon="search"/>} />
              <Card.Content>
              </Card.Content>
              <Card.Actions>
              </Card.Actions>
        </Card> 
      </View>
    );

  }
  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    right: 30,
    top: 120,
    left: 30,
    width : 300,
    backgroundColor: '#fff',
  },
});
