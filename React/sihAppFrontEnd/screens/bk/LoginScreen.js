import React from 'react';
import { TextInput } from 'react-native-paper';
import { Button } from 'react-native-paper';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';

import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Hospital In Hands ',
  };

  state = {
    text: '',
    text2: '',
    text1: ''
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
          </View>
          <View style={styles.getStartedContainer}>
            <View style={[styles.codeHighlightContainer, styles.homeScreenFilename]}>
            </View>
          </View>
          <View style={styles.helpContainer}>
            <TouchableOpacity onPress={this._handleHelpPress} style={styles.helpLink}></TouchableOpacity>
          </View>
        </ScrollView>
        <TextInput
          label="Login Id"
        />
        <TextInput
          label="Password"
          secureTextEntry={true}
        />
        <Button style={styles.loginButton} icon="exit-to-app" mode="contained" onPress={() => console.log('Pressed')}>
          Login 
        </Button>
      </View>

    );

  }
  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    right: 30,
    top: 120,
    left: 30,
    width : 300,
    backgroundColor: '#fff',
  },
  loginButton : {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0
  } 
});
