import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
// let json;
class CardDisplay extends React.Component {
    constructor(props) {
        super(props)
        console.log(this.props)
        this.state = {
          json : []
        }
    }
    componentDidMount(){
      fetch('http://192.168.43.100:2413/prescription', {
      method: 'post',
      headers: {'Content-Type':'application/json'},
      body:JSON.stringify(  {
        "key": this.props.query!=undefined?this.props.query:"5c8b824cf1dd20610bb7ae8c",
      })
      }).then(response => response.json())
      .then(data => {
          this.setState({
            json: data
          });
      })
    }
    render() {
      const data = this.state.json
        return (
            <View>                
                {this.state.json.map(item => 
                        <Card key={item._id}>
                          <Card.Content>
                            <Title>
                                Name: {item.patient[0]} ({item.metadata.patID})
                            </Title>
                            <Paragraph>
                                Age: {item.age} years{"\n"}                                
                                Doctor: {item.Doctor[0]}({item.metadata.docID}){"\n"}                                
                                Date: {item.date[0]}{"\n"}                                                              
                            </Paragraph>
                            <Paragraph>
                              Summary : {item.summary}{"\n"}  
                            </Paragraph>
                          </Card.Content>
                        </Card>
                    )} 
            </View>
        )
    }
}
export default CardDisplay


const styles = StyleSheet.create({
  id: {
    fontSize:17 
  },
  card: {
    borderBottomWidth: 1,
    borderTopWidth:1
  }
});