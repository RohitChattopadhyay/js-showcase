import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import CardDisplay from './patientDisplay.js'

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';
import { MonoText } from '../components/StyledText';
export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Patient Details',
  };
  state = {
    firstQuery: '',
  };

  render() {
    const { firstQuery } = this.state;
    return (
      <View>
        <Card onPress={() => this.props.navigation.navigate('Patient')}>
          <Card.Title title="Search Patients" left={(props) => <Avatar.Icon {...props}  icon ="person-pin"/>} />
          <Card.Content>
          </Card.Content>
        </Card>
        <Card onPress={() => this.props.navigation.navigate('Search')}>
          <Card.Title title="Search" left={(props) => <Avatar.Icon {...props} icon="search"/>} />
          <Card.Content>
          </Card.Content> 
          <Card.Actions>
          </Card.Actions>
        </Card> 
          {/* <FAB.Group */}
          {/*   open={this.state.open} */}
          {/*   icon={this.state.open ? 'clear' : 'menu'} */}
          {/*   actions={[ */}
          {/*     { icon: 'camera-alt', label: 'Capture Image ', onPress: () => console.log('Image Captured')}, */}
          {/*     { icon: 'picture-as-pdf', label: 'Upload PDF', onPress: () => console.log('PDF Uploaded') }, */}
          {/*   ]} */}
          {/*   onStateChange={({ open }) => this.setState({ open })} */}
          {/*   onPress={() => { */}
          {/*     if (this.state.open) { */}
          {/*       // do something if the speed dial is open */}
          {/*     } */}
          {/*   }} */}
          {/* /> */}
      </View>
    )
  }
  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    right: 30,
    top: 120,
    left: 30,
    width : 300,
    backgroundColor: '#fff',
  },
  main: {
    fontSize:25,
  }
});
