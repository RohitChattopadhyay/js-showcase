import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { withNavigation } from 'react-navigation';
class CardDisplay extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          query : '',
          content : []
        }
        this.handleSearch = this.handleSearch.bind(this)
    }
    componentDidMount(){
      fetch('http://192.168.43.100:2413/fileSearch', {
      method: 'post',
      headers: {'Content-Type':'application/json'},
      body:JSON.stringify(  {
        "key": ""
      })
      }).then(response => response.json())
      .then(data => {
          this.setState({
            content: data
          });
          console.log(data);
      })
    }
  
    handleSearch(q){
      fetch('http://192.168.43.100:2413/fileSearch', {
        method: 'post',
        headers: {'Content-Type':'application/json'},
        body:JSON.stringify(  {
          "key": q
        })
        }).then(response => response.json())
        .then(data => {
            this.setState({
              content: data
            });
            console.log(data,q);
        })
    }

    render() {
        return (
            <View>   
                <Searchbar
                  placeholder="Search"
                  onChangeText={que => { this.setState({ query: que });this.handleSearch(que)}}
                  value = {this.state.query} 
                />             
                {this.state.content.map(item => 
                        <Card key={item._id}  style={styles.card} onPress={()=>{this.props.navigation.push('PrescriptionDetails',{key:item._id})}}>
                          <Card.Content>
                                <Title>
                                    Date: {item.date[0]} 
                                </Title>
                                <Paragraph>
                                    Doctor: {item.Doctor[0]}{"\n"}
                                </Paragraph>
                          </Card.Content>
                        </Card>
                    )}              
            </View>
        )
    }
}
export default withNavigation(CardDisplay)


const styles = StyleSheet.create({
  id: {
    fontSize:17 
  },
  card: {
    borderBottomWidth: 1,
    borderTopWidth:1
  }
});