import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import CardDisplay from './patientDetailsDisplay.js'

import { withNavigation } from 'react-navigation';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';
import { MonoText } from '../components/StyledText';
let key;
class ShowScreen extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        search : '',
        query:''
      }
      try{
        key = this.props.navigation.state.params.key
      }
      catch(e){

      }
  }
  static navigationOptions = {
    title: 'Patient Records',
  };

  render() {
    return (
      <ScrollView style={{paddingBottom:40}}>
        <View>
          <CardDisplay query={key}/>        
        </View>
      </ScrollView>
    )
  }
  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

export default withNavigation(ShowScreen)
const styles = StyleSheet.create({
  container: {
    right: 30,
    top: 120,
    left: 30,
    width : 300,
    backgroundColor: '#fff',
  },
});