import React from 'react';  
import { TextInput } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import CardDisplay from './patientDisplay.js'

import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { WebBrowser } from 'expo';
import { MonoText } from '../components/StyledText';
export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'My Patients',
  };
  state = {
    search : '',
    content : []
  };
  handleSubmit(event){ 
    event.preventDefault();
    fetch('http://192.168.43.100:5000/allPatient', {
     method: 'post',
     headers: {'Content-Type':'application/json'},
     body: {
      "key": this.state.search
     }
    });
   };

  render() {
    const { search,content } = this.state;
    const contentA = [
      {
        "name" : "Priti",
        "age" : 20,
        "res" : "General",
        "id" : 15
      },
      {
        "name" : "Rohit ssssssssssssssssssssssssssssssssss",
        "age" : 19,
        "res" : "Why so serious",
        "id" : 20
      },
    ];
    return (
      <View>
        <Searchbar
          placeholder="Search"
          onChangeText={query => { this.setState({ search: query }); }}
          value = {search} 
        />
      <CardDisplay contentArr= {this.state.content} />
      </View>
    )
  }
  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    right: 30,
    top: 120,
    left: 30,
    width : 300,
    backgroundColor: '#fff',
  },
});
