import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LoginScreen from '../screens/LoginScreen';
import SearchScreen  from '../screens/SearchDetails';
import StaffScreen  from '../screens/StaffScreen';
import PatientScreen  from '../screens/PatientSearch';
import PatientDetailsScreen from '../screens/PatientDetails'
import PrescriptionDetailsScreen from '../screens/PrescriptionDetails'

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

const LinksStack = createStackNavigator({
  Links: LinksScreen,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};



const SearchStack = createStackNavigator({
  Search: SearchScreen,
});

SearchStack.navigationOptions = {
  tabBarLabel: 'Search',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'}
    />
  ),
};

// const LoginStack = createStackNavigator({
//   Links: LoginScreen,
// });
// 
// LoginStack.navigationOptions = {
//   tabBarLabel: 'Login',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
//     />
//   ),
// };


const PatientStack = createStackNavigator({
  Patient: PatientScreen,
});

PatientStack.navigationOptions = {
  tabBarLabel: 'Patient',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
};

const PatientDetailsStack = createStackNavigator({
  PatientDetails: PatientDetailsScreen,
});

PatientDetailsStack.navigationOptions = {
  tabBarLabel: ' ',
};

const PrescriptionDetailsStack = createStackNavigator({
  PrescriptionDetails: PrescriptionDetailsScreen,
});

PrescriptionDetailsStack.navigationOptions = {
  tabBarLabel: ' ',
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
};

export default createBottomTabNavigator({
  // export default  createStackNavigator({
  HomeStack,
  PatientDetailsStack,
  SearchStack,
  PrescriptionDetailsStack,
  PatientStack
},
{
  navigationOptions: {
    headerStyle: {
      paddingTop:0
    }
  }
}
);





/*
import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LoginScreen from '../screens/LoginScreen';
import SearchScreen  from '../screens/SearchDetails';
import StaffScreen  from '../screens/StaffScreen';
import PatientScreen  from '../screens/PatientSearch';
import PatientDetailsScreen from '../screens/PatientDetails'
import PrescriptionDetailsScreen from '../screens/PrescriptionDetails'

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-home'
      }
    />
  ),
};

const LinksStack = createStackNavigator({
  Links: LinksScreen,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};



const SearchStack = createStackNavigator({
  Search: SearchScreen,
});

SearchStack.navigationOptions = {
  tabBarLabel: 'Search',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};

// const LoginStack = createStackNavigator({
//   Links: LoginScreen,
// });
// 
// LoginStack.navigationOptions = {
//   tabBarLabel: 'Login',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
//     />
//   ),
// };


const PatientStack = createStackNavigator({
  Patient: PatientScreen,
});

PatientStack.navigationOptions = {
  tabBarLabel: 'Patient Details',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};


const PatientDetailsStack = createStackNavigator({
  PatientDetails: PatientDetailsScreen,
});

PatientDetailsStack.navigationOptions = {
  tabBarLabel: 'Patient Records',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};

const PrescriptionDetailsStack = createStackNavigator({
  PrescriptionDetails: PrescriptionDetailsScreen,
});

PrescriptionDetailsStack.navigationOptions = {
  tabBarLabel: 'Prescription Details',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};

export default createBottomTabNavigator({
  // export default  createStackNavigator({
  HomeStack,
  SearchStack,
  PatientStack,
  PatientDetailsStack,
  PrescriptionDetailsStack
},
{
  navigationOptions: {
    headerStyle: {
      paddingTop:0
    }
  }
}
);



*/