import navbarComponent  from "./components/navbar.js";
import footerComponent  from "./components/footer.js";
import homeComponent    from "./components/home.js";
import infoComponent    from "./components/info.js";
import linkComponent    from "./components/links.js";
import receiptComponent    from "./components/receipt.js";
import changePasswordComponent from "./components/changePassword.js";
import premiumComponent from "./components/premium.js";
import paymentComponent from "./components/payment.js";
import cashComponent    from "./components/cash.js";
import paytmComponent   from "./components/paytm.js";
import upiComponent     from "./components/upi.js";
import analyticsComponent     from "./components/analytics.js";

Vue.use(VueResource)
//const Foo = { template: '<div>foo</div>' }
let nameAPI;
const Bar = { template: '<div>bar</div>' }
window.planChoosen = "";
// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/profile', component: infoComponent },
  { path: '/links', component: linkComponent },
  { path: '/receipts', component: receiptComponent },
  { path: '/changePassword', component: changePasswordComponent },
  { path: '/analytics', component: analyticsComponent },
  { path: '/premium', component: premiumComponent },
  { path: '/premium/payment', component: paymentComponent ,name:"paymentGateway",props:true},
  { path: '/premium/payment/cash', component: cashComponent,name:"cashGateway",props:true },
  { path: '/premium/payment/paytm', component: paytmComponent ,name:"paytmGateway",props:true},
  { path: '/premium/payment/upi', component: upiComponent,name:"upiGateway",props:true },
  { path: '*', component: homeComponent}
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  el: '#app',
  data: {
      getData: [],
  },
  router,
  created: function (){
    this.fetchData();
  },
  methods:{
    fetchData: function(){
      this.$http.get('https://startups.juecell.in/api/startups/user/viewUser.php')
        .then(response => {
            this.getData = response.data;
            nameAPI = response.data;
            planChoosen = parseInt(response.data.plan);
          },
          (err) =>{
              window.location.replace("/admin.php");
          }
        )
    }
  },
  components: {
    navbarComponent,
    footerComponent
  },
  template:  `
    <div>
      <navbarComponent :name="getData.name" :type="getData.type"/>
      <div class="container"><router-view /></div>
      <footerComponent />
    </div>
  `,
});