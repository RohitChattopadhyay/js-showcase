export default {
	name : 'upiComponent',
	props: ['cost','txn'],
  data () {
    return {
    	loaded: false,
    	screenWidth : "",
    }
  },
	created: function (){
	    this.checkData();
	  },
	methods:{
	    checkData: function(){
	    	if (this.cost==undefined) this.$router.push('/premium/payment');
	      	this.loaded = true;
	      	this.screenWidth = window.innerWidth;	    
	    },
	    openUPIapp: function(){
	    	
	    	var url = "upi://pay?pa=rohit.chattopadhyay1@okhdfcbank&pn=JU ECell&am="+ this.cost +".00&tn=" + this.txn;
	    	window.open(url, '_blank');
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-container class="bv-example-row">
		<br>
		    <b-row class="justify-content-md-center">
		    	<b-col cols="12">
		    		<h4>UPI Payment</h4>
		    	</b-col>
		        <b-col md="4">
		        	Reference ID
		        	<h5>{{txn}}</h5>
		        	Amount to be Paid
		        	<h5>Rs. {{cost}}</h5>
		        </b-col>
		        <b-col md="8">
		        	<div class=".d-none .d-sm-block" v-if="screenWidth>560">
			        		<p class="centered">Scan to Pay</p>
			        		<center><img :src='"https://chart.googleapis.com/chart?cht=qr&chs=160x160&chld=L|0&chl=upi%3A%2F%2Fpay%3Fpa%3Drohit.chattopadhyay1%40okhdfcbank%26pn%3DJU+ECell%26am%3D" + cost + ".00%26tn%3D" + txn' style="max-width:90%"></center>
			        		<p class="centered"><b>rohit.chattopadhyay1@okhdfcbank</b><br>Rohit Chattopadhyay</p>
		        		
		        	</div>
		        	<b-btn class="mt-3" variant="success" block v-else v-on:click="openUPIapp">Click here to Pay</b-btn>
		        </b-col>
		        <b-col md="12"><hr>
		        	Instructions
		        	<ol>
		        		<li>Please note that the plan will not be activated unless we receive the money.</li>
		        		<li>Please note down the Reference ID.</li>
		        		<li>Email us or message us once you have paid the money if activation is delayed.</li>
		        	</ol>
		        	<small>
		        		We are not responsible for your money unless we have received the same.
		        	</small>
		        </b-col>
		    </b-row>
		</b-container>
	</div>
	`
};