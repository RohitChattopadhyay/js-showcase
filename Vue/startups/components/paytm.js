export default {
	name : 'paytmComponent',
	props: ['cost','txn'],
  data () {
    return {
    	loaded: false
    }
  },
	created: function (){
	    this.checkData();
	  },
	methods:{
	    checkData: function(){
	      if (this.cost==undefined) this.$router.push('/premium/payment');
	      this.loaded = true;
	    },
	    payTab: function(){
	    	var url;
	    	switch(parseInt(this.cost)){
	    		case 80:
	    			url = "https://google.com/80";
	    			break;
	    		case 50:
	    			url = "https://google.com/50";
	    			break;
	    	}
	    	window.open(url, '_blank');
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-container class="bv-example-row">
		<br>
		    <b-row class="justify-content-md">
		    	<b-col cols="12">
		    		<h4>Paytm Mode Payment</h4>
		    	</b-col>
		        <b-col md="12">
		        	Reference ID
		        	<h5>{{txn}}</h5>
		        </b-col>
		        <b-col md="12">
		        	Amount to be Paid
		        	<h5>Rs. {{cost}}</h5>
		        </b-col>
		        <b-col md="12">
		        	<b-btn class="mt-3" variant="success" block v-on:click="payTab">Click here to Pay</b-btn>
		        </b-col>
		        <b-col md="12"><hr>
		        	Instructions
		        	<ol>
		        		<li>Please note that the plan will not be activated unless we receive the money.</li>
		        		<li>Please note down the Reference ID.</li>
		        		<li>Email us or message us once you have paid the money if activation is delayed.</li>
		        	</ol>
		        	<small>
		        		We are not responsible for your money unless we have received the same.
		        	</small>
		        </b-col>
		    </b-row>
		</b-container>
	</div>
	`
};