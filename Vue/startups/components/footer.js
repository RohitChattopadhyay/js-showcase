export default {
	name: 'footerComponent',
	template: `
	    <b-navbar toggleable="lg" type="light" variant="light" fixed="bottom" :sticky="false">
	    	<b-collapse is-nav id="footer-links">
	   			<b-navbar-nav>
   					<b-nav-item href="#">Terms & Conditions</b-nav-item>
   					<b-nav-item href="#">Privacy Policy</b-nav-item>
   					<b-nav-item href="#">Developers</b-nav-item>
	   			</b-navbar-nav>
	   		</b-collapse is-nav>
   			<b-navbar-nav class="ml-auto">
				<b-nav-text>© Jadvpur University ECell</b-nav-text>
			</b-navbar-nav>
		</b-navbar>
	`
};