export default {
	name : 'homeComponent',
  data () {
    return {
    	name : "",
    	loaded: false
    }
  },
	created: function (){
	    this.fetchData();
	  },
	methods:{
	    fetchData: function(){
	      this.$http.get('https://startups.juecell.in/api/startups/user/viewUser.php')
	        .then(response => {
	            this.name = response.data.name;
	            this.loaded = true;
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    },
	    goLinks: function(){
	    	this.$router.push('/links');
	    },
	    goAnalytics: function(){
	    	this.$router.push('/analytics');
	    },
	    goProfile: function(){
	    	this.$router.push('/profile');
	    },
	  },
	template: `
	<div v-if="loaded">
		<b-jumbotron :header="'Welcome ' + name.split(' ')[0]" lead="Get your venture Online" id="jumbo-home" >
		</b-jumbotron>
		<b-container class="bv-example-row centered">
		    <b-row class="justify-content-md-center">
		        <b-col v-on:click="goLinks" class="home-tabs col-sm-3 "><i class="fas fa-link"></i><h4>Links</h4></b-col>
		        <b-col v-on:click="goAnalytics" class="home-tabs col-sm-3 "><i class="fas fa-chart-line"></i><h4>Analytics</h4></b-col>
		        <b-col v-on:click="goProfile" class="home-tabs col-sm-3 "><i class="fas fa-info"></i><h4>Info</h4></b-col>
		    </b-row>
		</b-container>
	</div>
	`
};