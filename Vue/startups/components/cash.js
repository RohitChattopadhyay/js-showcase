export default {
	name : 'cashComponent',
	props: ['cost','txn'],
  data () {
    return {
    	loaded: false
    }
  },
	created: function (){
	    this.checkData();
	  },
	methods:{
	    checkData: function(){
	      if (this.cost==undefined) this.$router.push('/premium/payment');
	      this.loaded = true;
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-container class="bv-example-row">
		<br>
		    <b-row class="justify-content-md">
		    	<b-col cols="12">
		    		<h4>Cash Mode Payment</h4>
		    	</b-col>
		        <b-col md="12">
		        	Reference ID
		        	<h5>{{txn}}</h5>
		        </b-col>
		        <b-col md="12">
		        	Amount to be Paid
		        	<h5>Rs. {{cost}}</h5>
		        </b-col>
		        <b-col md="12"><hr>
		        	Instructions
		        	<ol>
		        		<li>Please note that the plan will not be activated unless we receive the money.</li>
		        		<li>Pay the amount citing the above mentioned Reference ID to an ECell Coordinator.</li>
		        		<li>You may message us on our Facebook page to get details of an ECell Coordinator.</li>
		        		<li>Please take the name and mobile number of the person whom you are paying.</li>
		        		<li>Email us or message us once you have given the money to our representative.</li>
		        	</ol>
		        	<small>
		        		Please ensure the identity of the person whom you are giving the money, also make use that the person is associated with ECell.<br>Do not pay to any unknown person without confirming the identity and association with ECell.<br>We are not responsible for your money.
		        	</small>
		        </b-col>
		    </b-row>
		</b-container>
	</div>
	`
};