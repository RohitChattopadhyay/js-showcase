export default {
	name: 'navbarComponent',
	props: [ 'name' , 'type'],
	template: `
	    <b-navbar toggleable="md" type="dark" variant="primary" :sticky = true fixed = "top">
			<b-navbar-toggle target="nav_collapse"></b-navbar-toggle>
			<b-navbar-brand to="/"><b>Startups</b><small><sup>β</sup></small></b-navbar-brand>
			<b-collapse is-nav id="nav_collapse">
				<b-navbar-nav>
				  <b-nav-item to="/links">Links</b-nav-item>
				  <b-nav-item to="/analytics" v-if="type == '0'">Analytics</b-nav-item>
				  <b-nav-item to="/receipts">Receipts</b-nav-item>
				  <b-nav-item to="/premium" v-if="type == '0'">Premium</b-nav-item>
				</b-navbar-nav>
				<!-- Right aligned nav items -->
				<b-navbar-nav class="ml-auto">
						<b-nav-item-dropdown right>
								<template slot="button-content">
								<b-nav-text>Welcome</b-nav-text>					
								  {{name}}
								</template>
								<b-dropdown-item to="/profile">Profile</b-dropdown-item>
							<b-dropdown-item to="/changePassword">Change Password</b-dropdown-item>
							<b-dropdown-item href="destroy.php">Signout</b-dropdown-item>
						</b-nav-item-dropdown>
				</b-navbar-nav>
			</b-collapse>
		</b-navbar>
	`
};