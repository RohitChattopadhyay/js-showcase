export default {
	name : 'receiptComponent',
  data () {
    return {
    	getData : [],
    	loaded: false,
    	items: [],
    	daysRemain : "",  
    	currentCount: null,    
    	sortBy: 'receiptID',
      	sortDesc: true,
       	fields: [
	        { key: 'receiptID', label: 'Receipt ID' ,sortable: true },
	        { key: 'date', label: 'Date' ,sortable: false },
	        { key: 'amount', label: 'Paid Amount', sortable: true },
	        { key: 'planId', label: 'Plan ID'},
	        { key: 'mode', label: 'Payment Mode'},
	      ],
    }
  },
	created: function (){
	    this.fetchData();
	  },
	methods:{
	    fetchData: function(){
	      this.$http.get('https://startups.juecell.in/api/startups/user/showReceipt.php')
	        .then(response => {
	            this.getData = response.data;
	            this.loaded = true;
	            this.currentCount = response.data.count;
	            this.items = response.data.result[0];
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    },
	  },
	template: `
	<div v-if="loaded">
		<b-jumbotron header="Receipts" id="jumbo-receipts" >
		</b-jumbotron>
		<b-container class="bv-example-row" id="profile-content">
		    <b-row class="justify-content-md-center">
		        <b-col>
		        	<b-container>
		        		<b-row>
		        			<small><em>Recent transactions will take time to reflect here.</em></small>     		
		        			<b-col cols="12"><br>
		        				<b-table v-if="items.length>0" :hover=true :sort-by.sync="sortBy"
						         	:sort-desc.sync="sortDesc"
						         	:fixed="true"
						            :items="items"
						            :fields="fields"
						            :responsive=true>
						           	<template slot="amount" slot-scope="data">
								     Rs. {{data.item.amount}}
								    </template>
								    <template slot="mode" slot-scope="data">
								    	{{data.item.mode.toUpperCase()}}
								    </template>
						        </b-table>
						        <h3 v-else>No Records found</h3>
						        </b-col>
			        	</b-row>
			        </b-container>
		        </b-col>
		    </b-row>
		</b-container>
	</div>
	`
};