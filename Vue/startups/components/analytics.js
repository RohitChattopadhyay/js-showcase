export default {
	name : 'analyticsComponent',
  data () {
    return {
    	loaded: false,
    	graphLoad :false,
    	error : false,
    	msg: "Loading..."
    }
  },
	created: function (){
	    this.fetchData();
	  },
	methods:{
	    fetchGraph: function(filter,accessToken){
	    	gapi.analytics.ready(function() {
	    		
	    		gapi.analytics.auth.authorize({
			        'serverAuth': {
			          'access_token': accessToken
			        }
			      }); 
	    		  var dataChartLine = new gapi.analytics.googleCharts.DataChart({
				    query: {
				      'ids': 'ga:172063570', // <-- Replace with the ids value for your view.
				      'start-date': '30daysAgo',
				      'end-date': 'yesterday',
				      'metrics': 'ga:Pageviews',
				      'dimensions': 'ga:date',      
				      'filters': filter
				    },
				    chart: {
				      'container': 'chart-1-container',
				      'type': 'LINE',
				      'options': {
				        'width': '100%'
				      }
				    }
				  });

	    		  var dataChartTable = new gapi.analytics.googleCharts.DataChart({
				    query: {
				      'ids': 'ga:172063570',
				      'start-date': '30daysAgo',
				      'end-date': 'yesterday',
				      'metrics': 'ga:pageviews,ga:uniquePageviews,ga:organicSearches,ga:avgTimeOnPage,ga:sessionsPerUser',
				      'dimensions': 'ga:pagePath',      
				      'include-empty-rows': true,
				      'filters': filter
				    },
				    chart: {
				      'container': 'chart-2-container',
				      'type': 'TABLE',
				      'options': {
				        'width': '100%'
				      }
				    }
				  });
				  var errorCount = 0;
				  dataChartTable.on('success', function(response) {	              		
	              		if(errorCount) document.getElementById('chart-1-container').innerHTML = this.msg;
	              		else this.graphLoad = true;	
					});			
				  dataChartTable.on('error', function(response) {
	              		this.graphLoad = false;
	              		this.msg = "Error, please try again later or contact support.";
	              		errorCount++;
	              		if(errorCount) document.getElementById('chart-1-container').innerHTML = this.msg;
	              		console.log(response.error.message);
					});
				  dataChartLine.on('success', function(response) {	              		
	              		if(errorCount) document.getElementById('chart-1-container').innerHTML = this.msg;
	              		else this.graphLoad = true;
					});			
				  dataChartLine.on('error', function(response) {
	              		this.graphLoad = false;
	              		this.msg = "Error, please try again later or contact support.";	              		
	              		errorCount++;
	              		if(errorCount) document.getElementById('chart-1-container').innerHTML = this.msg;
	              		console.log(response.error.message);
					});
				  dataChartLine.execute();
				  dataChartTable.execute();

	    	});
	    },
	    fetchString: function(aT){
	      this.$http.get('https://startups.juecell.in/api/startups/user/showLinks.php')
	        .then(response => {
	            if(response.data.count>0){
	            	this.fetchGraph(response.data.filter,aT);
	            }
	            else{
	            	 document.getElementById('chart-1-container').innerHTML = "Please add Links, analytics will be shown within few days of link addition.";
	            }	            
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    },
	    fetchData: function(){
	      this.$http.get('https://startups.juecell.in/api/analytics/token.php')
	        .then(response => {
	        	this.loaded = true;
	            this.fetchString(response.data.token);
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    }
	  },
	template: `
	<div v-if='this.loaded'>
		<b-jumbotron header="Analytics" id="jumbo-analytics" lead="Your 30 days statistics">
		</b-jumbotron>
		<b-container class="bv-example-row" id="analytics-content">
			<b-row class="justify-content-md-center" v-if="this.error">
		    </b-row>
		    <b-row class="justify-content-md-center">
		        <b-col sm="12">
		        	<div id="chart-1-container">{{msg}}</div>
		        </b-col>
		        <b-col sm="12">
		        	<div id="chart-2-container"></div>
		        </b-col>
		    </b-row>
		</b-container>
	</div>
	`
};