export default {
	name : 'linkComponent',
  data () {
    return {
    	form : {
    		link : "",
    		target: "",
    	},
    	editForm : {
    		link : "",
    		target : ""
    	},
    	getData : [],
    	loaded: false,
    	items: [],
    	alert: {
    		msg : null,
    		variant : null, 
      		showDismissibleAlert: false,
    	},
    	daysRemain : "",  
    	currentCount: null,    
    	sortBy: 'links',
      	sortDesc: false,
      	fields: [
	        { key: 'links', label: 'Link' ,sortable: true },
	        { key: 'target', label: 'Link to Code' ,sortable: false },
	        { key: 'hit', label: 'Hits', sortable: true },
	        { key: 'Actions', label: ''}
	      ],
    }
  },
	created: function (){
	    this.fetchData();
	  },
	methods:{
	    fetchData: function(){
	      this.$http.get('https://startups.juecell.in/api/startups/user/showLinks.php')
	        .then(response => {
	            this.getData = response.data;
	            this.loaded = true;
	            this.currentCount = response.data.count;
	            this.items = response.data.result[0];
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    },
	    addLink: function(){
	    	event.preventDefault();
	     	if(this.form.target.length<5){
	    		    this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Enter proper target URL, try again.";
	    			this.hideModal();
    				return;
	    	}
	    	var formData = new FormData();
	    	formData.append('link',this.form.link);
	    	formData.append('target',this.form.target);
	    	this.$http.post("https://startups.juecell.in/api/startups/user/addLinks.php",formData)
	    		.then(response => {
	    			if (response.data.success==1) {
	    				this.currentCount++;
	    				var obj = {};
	    				obj["hit"] = 0;
	    				obj["links"] = response.data.link;
	    				obj["target"] = response.data.target;
	    				this.items.push(obj);
	    				this.form.link 	=	"";
	    				this.form.target=	"";
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="success";
	    				this.alert.msg=response.data.message;
	    				document.getElementById("addLinksForm").reset();
	    			} else {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="danger";
	    				this.alert.msg=response.data.message;
	    			}
	    		},
	    		(err) =>{
    				this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Server error, try again later";
	    		}
	    		)
	    },
	   	editLink: function(){
	    	event.preventDefault();
	    	if(this.editForm.target.length<5){
	    		    this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Enter proper target URL, try again.";
	    			this.hideModal();
    				return;
	    	}
	    	var formData = new FormData();
	    	formData.append('link',this.editForm.link);
	    	formData.append('target',this.editForm.target);
	    	this.$http.post("https://startups.juecell.in/api/startups/user/editLinks.php",formData)
	    		.then(response => {
	    			if (response.data.success==1) {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="success";
	    				this.alert.msg="Link successfully updated";
	    				this.fetchData();
	    			} else {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="danger";
	    				this.alert.msg=response.data.message;
	    			}
	    			this.hideModal();
	    		},
	    		(err) =>{
    				this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Server error, try again later";
	    		}
	    	)
	    },
	    showModal (t) {
	    	this.editForm.link = t;
	    	this.editForm.target = "";
      		this.$refs.myModalRef.show();
	    },
	    hideModal () {
	      this.$refs.myModalRef.hide()
	    },
	    deleteLink (l){
	    	if(!confirm("Are you sure, you want to delete "+l))	return;
	    	var formData = new FormData();
	    	formData.append('link',l);
	    	this.$http.post("https://startups.juecell.in/api/startups/user/removeLinks.php",formData)
	    			.then(response => {
	    			if (response.data.success==1) {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="success";
	    				this.alert.msg="Link removed successfully";
	    				this.fetchData();
	    			} else {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="danger";
	    				this.alert.msg=response.data.message;
	    			}
	    			this.hideModal();
	    		},
	    		(err) =>{
    				this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Server error, try again later";
	    		}
	    	)
	    },
	    copyLink (l){
	    	document.getElementById('copyHelpInput').value = l;
	        document.getElementById('copyHelpInput').select();
	        document.execCommand('copy');
	        window.getSelection().removeAllRanges();
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-jumbotron header="Links" id="jumbo-links" >
		</b-jumbotron>
		<b-container class="bv-example-row" id="link-content">
		    <b-row class="justify-content-md-center">
		        <b-col>
		        	<b-container>
		        		<b-row>
		   				    <b-alert class="col-12" :variant="alert.variant"
						             dismissible
						             :show="alert.showDismissibleAlert"
						             @dismissed="alert.showDismissibleAlert=false">
						      {{alert.msg}}
						    </b-alert>		        		
		        			<b-col cols="12" v-if="getData.limit>currentCount" >
		        				<h4>Add Link</h4>
		        				<form id="addLinksForm">
			        			<div class="input-group">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id="" v-if="items.length>0">{{items[0].links}}/</span>
									<span class="input-group-text" id="" v-else>/</span>
								  </div>
								  <input required type="text" v-model="form.link" class="form-control" placeholder="Link">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id=""><i class="fas fa-arrow-left"></i></span>
								  </div>
								  <input required class="form-control" v-model="form.target" type="url" placeholder="Code Link">
								  <b-button type="submit" v-if="form.link.length*form.target.length>0" v-on:click="addLink()" style="border-bottom-left-radius:0;border-top-left-radius:0"><i class="fas fa-plus-circle"></i> Add</b-button>
								</div>
								</form>
		        			</b-col>
		        			<b-col v-else>
		        				<small><b-link style="text-decoration:none" to="/premium">Upgrade</b-link> your pack to add more links.</small>
		        			</b-col>
		        			<b-col cols="12"><br>
		        				<b-table v-if="currentCount>0" :hover=true :sort-by.sync="sortBy"
						         	:sort-desc.sync="sortDesc"
						         	:fixed="false"
						            :items="items"
						            :fields="fields"
						            :responsive=true>
						            <template slot="Actions" slot-scope="data">
								     <div class="btn-group btn-group-sm" role="group" aria-label="Tools">
									  <button type="button" class="btn btn-secondary" v-b-popover.hover="'Edit link'" v-on:click="showModal(data.item.links)"><i class="fas fa-edit"></i></button>
									  <button type="button" class="btn btn-secondary" v-b-popover.hover="'Delete link'" v-if="data.index>0" v-on:click="deleteLink(data.item.links)"><i class="fas fa-times"></i></button>
									</div> 
								    </template>
						            <template slot="links" slot-scope="data">
								     <a style="text-decoration:none" :href="'https://startups.juecell.in'+data.item.links" target="_blank">{{data.item.links}}</a>
								    </template>
								    <template slot="target" slot-scope="data">
								    	<span :title=data.item.target>{{(data.item.target.length>50)?(data.item.target.substr(0,50)+"..."):data.item.target}}</span> <i class="copier far fa-copy" v-b-popover.hover="'Copy'" v-on:click="copyLink(data.item.target)"></i>
								    </template>
								    <template slot="table-caption">
								      <small><b>Note:</b> You cannot delete root link once created.
								      <br>&emsp;<em>Hits</em> is the number of times the link has been accessed.</small>
								    </template>
						        </b-table>
						        </b-col>
			        	</b-row>
			        </b-container>
		        </b-col>
		    </b-row>
		</b-container>
		<div>
		    <b-modal ref="myModalRef" hide-footer title="Edit Link">
		      <div class="d-block">
		      	<b-container fluid>
		      		<b-row>
		      			<b-col cols="12"><small>Link:</small></b-col>
						<b-col cols="12"><h5>{{editForm.link}}</h5></b-col>
						<b-col cols="12"><small>Link to code:</small></b-col>
						<b-col cols="12"><input required class="form-control" v-model="editForm.target" type="url" placeholder="Code Link"></b-col>
		      		</b-row>
		      	</b-container>
		      </div>
            <b-btn class="mt-3" variant="outline-primary" block @click="editLink">Update Link</b-btn>
		    </b-modal>
		</div>
		<input id="copyHelpInput">
	</div>
	`
};