export default{
	name : `changePasswordComponent`,
	data() {
		return{
			currentPass : null,
			newPass : null,
			newRePass: null,
		 	alert: {
	    		msg : null,
	    		variant : null, 
	      		showDismissibleAlert: false,
	    	},
		}
	},
	methods:{
		changePassword: function(){
		 	event.preventDefault();
	    	if(this.currentPass.length==0){
	    		    this.alert.showDismissibleAlert=true;
    				this.alert.variant="warning";
    				this.alert.msg="Please fill all fields.";
    				return;
	    	}	    	
	    	if(this.newPass.length<8){
	    		    this.alert.showDismissibleAlert=true;
    				this.alert.variant="warning";
    				this.alert.msg="Enter stronger password.";
    				return;
	    	}	    	
	    	if(this.currentPass==this.newRePass){
	    		    this.alert.showDismissibleAlert=true;
    				this.alert.variant="warning";
    				this.alert.msg="New and current password cannot be same.";
    				return;
	    	}	    	
	    	if(this.newPass!=this.newRePass){
	    		    this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="New password and re entered passwords must match.";
    				return;
	    	}
	    	var formData = new FormData();
	    	formData.append('password',this.currentPass);
	    	formData.append('newPassword',this.newPass);
	    	this.$http.post("https://startups.juecell.in/api/startups/auth/passwordChange.php",formData)
	    		.then(response => {
	    			if (response.data.success==1) {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="success";
	    				this.alert.msg="Password successfully updated";
	    			} else {
	    				this.alert.showDismissibleAlert=true;
	    				this.alert.variant="danger";
	    				this.alert.msg=response.data.message;
	    			}
	    		},
	    		(err) =>{
    				this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Server error, try again later";
	    		}
	    	)
		}
	},
	template: `
	<div>
		<b-jumbotron header="Password" id="jumbo-password" >
		</b-jumbotron>
		<b-container class="bv-example-row" id="profile-content">
		    <b-row class="justify-content-md-center">
		        <b-col>
		        	<b-container>
		        		<b-row>
		   				    <b-alert class="col-12" :variant="alert.variant"
						             dismissible
						             :show="alert.showDismissibleAlert"
						             @dismissed="alert.showDismissibleAlert=false">
						      {{alert.msg}}
						    </b-alert>		        		
		        			<b-col cols="12">
		        				<p>Current Password</p>
		        				<input required class="col-12 form-control" v-model="currentPass" type="password" placeholder="Enter your current Password">
		        				<br>
		        				<p>New Password</p>
		        				<input required class="col-12 form-control" v-model="newPass" type="password" placeholder="Enter new Password">
								<br>
		        				<p>Re-enter New Password</p>
		        				<input required class="col-12 form-control" v-model="newRePass" type="password" placeholder="Re-Enter new Password">
		        				<hr>
		        				<b-col>
		        				   <b-button variant="primary" style="float:right" v-on:click="changePassword()">Change Password</b-button>
		        				</b-col>
		        			</b-col>
			        	</b-row>
			        </b-container>
		        </b-col>
		    </b-row>
		</b-container>
	</div>
	`
};