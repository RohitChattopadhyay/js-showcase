export default {
	name : 'infoComponent',
  data () {
    return {
    	getData : [],
    	loaded: false,
    	daysRemain : ""
    }
  },
	created: function (){
	    this.fetchData();

	  },
	methods:{
	    fetchData: function(){
	      this.$http.get('https://startups.juecell.in/api/startups/user/viewUser.php')
	        .then(response => {
	            this.getData = response.data;
	            this.loaded = true;
	           	this.daysRemaining();
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    },
	    daysRemaining: function(){
			var date1 = moment();
			var date2 = moment(this.getData.planExpire);
			var diff = date2.diff(date1,"days");
			if(diff>0) this.daysRemain = diff + " days";
	    	else this.daysRemain = "Expired";
	    },
	    goPremium: function(){
	    	console.log("hi");
	    	this.$router.push('/premium');
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-jumbotron header="Profile" id="jumbo-profile" >
		</b-jumbotron>
		<b-container class="bv-example-row" id="profile-content">
		    <b-row class="justify-content-md-center">
		        <b-col sm="9">
		        	<b-container>
		        		<b-row>
		        			<b-col cols="12" class="profile-data">
					        	<b-col><small>Name</small></b-col>
					        	<b-col><h5>{{getData.name}}</h5></b-col>
				        	</b-col>
		        			<b-col cols="12" class="profile-data" v-if='getData.startup.length>0'>
					        	<b-col><small>Venture Name</small></b-col>
					        	<b-col><h5>{{getData.startup}}</h5></b-col>
				        	</b-col>
		        			<b-col cols="12" class="profile-data">
					        	<b-container>
			        				<b-row cols="12">
						        		<b-col sm="9"><small>Email</small><h5>{{getData.email}}</h5></b-col>
						        		<b-col sm="3"><small>Mobile Number</small><h5>{{getData.mob}}</h5></b-col>
						        	</b-row>
						        </b-container>
				        	</b-col>
		        			<b-col cols="12" class="profile-data">
		        			<hr>
		        				<b-col cols="12"><h5>Plan Details</h5></b-col>
		        				<b-container>
			        				<b-row cols="12">
						        		<b-col cols="2"><h6>PID</h6><h5>{{getData.plan}}</h5></b-col>
						        		<b-col cols="7"><h6>Plan Name</h6><h5>{{getData.planName}}</h5></b-col>
						        		<b-col sm="3" v-if="getData.plan!='1900'"><h6>Expires on</h6><h5>{{getData.planExpire}}</h5></b-col>
						        	</b-row>
						        </b-container>
				        	</b-col>
			        	</b-row>
			        </b-container>
		        </b-col>
	        	<b-col sm="3" class="centered">
	        		<b-card v-if="getData.plan=='1900'" title="Premium"
					          tag="article"
					          style="max-width: 20rem;padding: 2rem 0"
					          class="mb-2">
					    <b-button class="w-100" variant="success" v-on:click='this.goPremium'>Get Now</b-button>
					</b-card>
	        		<b-card v-else title="To Expire"
					          :img-src="'https://dummyimage.com/600x300/ffffff/000000&text=+' + daysRemain"
					          img-alt="Image"
					          img-top
					          tag="article"
					          border-variant="secondary"
					          style="max-width: 20rem;"
					          class="mb-2">
					    <b-button class="w-100" variant="success" v-on:click='this.goPremium'>Renew</b-button>
					</b-card>
	        	</b-col>
		    </b-row>
		</b-container>
	</div>
	`
};