export default {
	name : 'premiumComponent',
  data () {
    return {
    	name : "",
    	getData : [],
    	loaded: false
    }
  },
	created: function (){
	    this.fetchData();
	  },
	methods:{
	    fetchData: function(){
	      this.$http.get('https://startups.juecell.in/api/startups/user/viewUser.php')
	        .then(response => {
	            this.name = response.data.name;
	            this.getData = response.data;
	            this.loaded = true;
	          },
	          (err) =>{
	              window.location.replace("/admin.php");
	          }
	        )
	    },
	    packSelector: function(id){
	    	//planChoosen = id;
	    	var formData = new FormData();
	    	formData.append('plan',id);
	    	this.$http.post("https://startups.juecell.in/api/startups/user/genTempReceipt.php",formData)
	    		.then(response => {
	    			if (response.data.success==1) {	    				
	    				this.$router.push({ name: 'paymentGateway', params: { dataArr: response.data } });
	    			} else {
	    				alert(response.data.message);
	    			}
	    		},
	    		(err) =>{
    				this.alert.showDismissibleAlert=true;
    				this.alert.variant="danger";
    				this.alert.msg="Server error, try again later";
	    		}
	    	)

	    },
	    showButton:function(id){
	    	return this.getData.plan!=id;
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-jumbotron header="Premium" lead="" id="jumbo-premium" >
		</b-jumbotron>
		<b-container class="bv-example-row centered">
		    <b-row class="justify-content-md-center">
		    	<h1>Pricing Plans</h1>
		    </b-row>
		    <b-row class="justify-content-md-center">
		        <b-col v-on:click="" class="premium-tabs col-sm-3 ">
		        	<i class="fas fa-rocket"></i>
		        	<h3>Free</h3>
		        	<h5> &nbsp;</h5>	
		        	<div class="premium-feature"> 
		        		<ul>
		        			<li>Unlimted Validity</li>
		        			<li>2 links</li>
		        			<li>Unlimted Bandwidth</li>
		        		</ul>
		        	</div>
		        	<b-btn class="mt-3 btn-block" v-if='!showButton(1900)' variant="outline-primary" style="cursor:default" >Current Plan</b-btn>
		        </b-col>
		        <b-col v-on:click="" class="premium-tabs col-sm-3 ">
		        	<i class="fas fa-heart"></i>
		        	<h3>Yearly</h3>
		        	<h5>Rs.80</h5>	
		        	<div class="premium-feature"> 
		        		<ul>
		        			<li>360 days validity</li>
		        			<li>5 links</li>
		        			<li>Unlimted Bandwidth</li>
		        		</ul>
		        	</div>
		        	<b-btn class="mt-3" v-if='showButton(1912)' variant="outline-primary" block @click='packSelector(1912)'>Get Now</b-btn>
		        	<b-btn class="mt-3 btn-block" v-else variant="outline-primary" @click='packSelector(1912)'>Renew</b-btn>
		        </b-col>
		        <b-col v-on:click="" class="premium-tabs col-sm-3 ">
		        	<i class="fas fa-magic"></i>
		        	<h3>Monthly</h3>
		        	<h5>Rs.50</h5>	
		        	<div class="premium-feature"> 
		        		<ul>
		        			<li>30 days validity</li>
		        			<li>5 links</li>
		        			<li>No Ads</li>
		        			<li>Unlimted Bandwidth</li>
		        		</ul>
		        	</div>
		        	<b-btn class="mt-3" v-if='showButton(1901)' variant="outline-primary" block @click='packSelector(1901)'>Get Now</b-btn>
		        	<b-btn class="mt-3 btn-block" v-else variant="outline-primary" @click='packSelector(1901)'>Renew</b-btn>
		        </b-col>
		        <b-col cols="12" style="margin-bottom:5em"><small>Contact us if you want to get your website made by us.</small></b-col>
		    </b-row>
		</b-container>
	</div>
	`
};