export default {
	name : 'paymentComponent',
	props: ['dataArr'],
  data () {
    return {
    	loaded: false
    }
  },
	created: function (){
	    this.checkData();
	  },
	methods:{
	    checkData: function(){
	    	this.loaded = true;
	    	if(this.dataArr==undefined) this.$router.push('/premium');
	    },
	    chooseMode: function(t) {
	    	switch(t){
	    		case 0:
	    		//cash
	    			this.$router.push({ name: 'cashGateway' , params: { cost: this.dataArr.cost, txn: this.dataArr.txnid }});
	    			break;
	    		case 1:
	    		//UPI
	    			this.$router.push({ name: 'upiGateway'  , params: { cost: this.dataArr.cost, txn: this.dataArr.txnid }});
	    			break;
	    		case 2:
	    		//paytm
	    			this.$router.push({ name: 'paytmGateway', params: { cost: this.dataArr.cost, txn: this.dataArr.txnid }});
	    			break;
	    	}
	    }
	  },
	template: `
	<div v-if="loaded">
		<b-jumbotron header="Payment" lead="" id="jumbo-payment" >
		</b-jumbotron>
		<b-container class="bv-example-row"  v-if='dataArr.cost>0'>
		    <b-row class="justify-content-md">
		    	<b-col cols="12">
		    		<h4>Plan Details</h4>
		    	</b-col>
		        <b-col md="6">
		        	Name
		        	<h5>[{{dataArr.planId}}]{{dataArr.planName}}</h5>
		        </b-col>
		        <b-col md="3">
		        	Validity
		        	<h5>{{dataArr.days}} days</h5>
		        </b-col>
		        <b-col md="3">
		        	Amount to Pay
		        	<h5>Rs {{dataArr.cost}}</h5>
		        </b-col>
		      	<b-col cols="12">
		      		<hr>
		    		Choose Payment Mode
		    	</b-col>
		    </b-row>
		    <b-row align-h="center" class="centered payModesSelect">
		        <b-col class="pay-modes col-md-3" v-on:click='chooseMode(1)'>
		        	<img src="https://i.imgur.com/ziercwA.png" class="pay-logo" alt="UPI"><br>
		        	<small>G-Pay(Tez), PhonePe, BHIM etc UPI</small>
		        </b-col>
		        <b-col class="pay-modes okGoogle col-md-3" v-on:click='chooseMode(2)'>
		        	<img src="https://i.imgur.com/GU020aQ.png" class="pay-logo" alt="paytm"><br>
		        	<small>Only for non UPI paytm users</small>
		        </b-col>
		        <b-col class="pay-modes col-md-3" v-on:click='chooseMode(0)'>
		        	<img src="https://i.imgur.com/v5YGFi9.png" class="pay-logo" alt="">
		        	Cash<br>
		        	<small>Submitting to an ECell Cordinator</small>
		        </b-col>
		    </b-row>
		</b-container>
		<b-container class="bv-example-row"  v-else>
		    <b-row class="justify-content-md">
		    	<b-col cols="12" class="centered">
		    		<h4>Thank You</h4>
		    		{{dataArr.message}}
		    	</b-col>
		    </b-row>
		</b-container>
	</div>
	`
};