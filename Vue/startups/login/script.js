  var options = {
    'btn-loading': '<i class="fa fa-spinner fa-pulse"></i>',
    'btn-success': '<i class="fa fa-check"></i>',
    'btn-error': '<i class="fa fa-remove"></i>',
    'msg-success': 'Login success! Redirecting...',
    'msg-error': 'Wrong login credentials!',
    'useAJAX': true,
  };
function remove_loading(form)
{
  form.find('[type=submit]').removeClass('error success');
  form.find('.login-form-main-message').removeClass('show error success').html('');
}

function form_loading(form)
{
  form.find('[type=submit]').addClass('clicked').html(options['btn-loading']);
}

function form_success(form,msg)
{
  form.find('[type=submit]').addClass('success').html(options['btn-success']);
  form.find('.login-form-main-message').addClass('show success').html(msg);
}

function form_failed(form,msg)
{
  form.find('[type=submit]').addClass('error').html(options['btn-error']);
  form.find('.login-form-main-message').addClass('show error').html(msg);
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

$("#login-form").submit(function(e) {
    var form = $(this);
    var url = "https://startups.juecell.in/api/startups/auth/login.php";
    remove_loading(form);
    form_loading(form);
    var data = form.serializeArray();
    data.push({sessID: 	getCookie('PHPSESSID')});
    $.ajax({
           type: "POST",
           url: url,
           data: data, // serializes the form's elements.
           success: function(data)
           {
               if(data.success == 0){
                   console.log(data.token);
                   document.cookie = "PHPSESSID=" + data.token;
                   console.log(document.cookie);
                form_failed(form,data.message);
               }
               else if(data.success == 1){
                   //document.cookie = "PHPSESSID=" + data.token;
                   //console.log(document.cookie);
                form_success(form,data.message);
                setTimeout(function(){ window.location = "./dashboard/"; }, 500);
               }
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});

$("#forgot-password-form").submit(function(e) {
    var form = $(this);
    var url = "https://startups.juecell.in/api/startups/auth/passwordForgot.php";
    remove_loading(form);
    form_loading(form);
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(response)
           {  
            form_success(form,"Recovery Email sent");            
               // if(response.success = 202){
               //  form_failed(form,"Err Contact support");
               // }
               // else {
               //  form_success(form,"Recovery Email sent");
               // }
           },
           error: function(response){
            form_failed(form,"Error Contact support");
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});

$("#register-form").submit(function(e) {
    var form = $(this);
    var url = "https://startups.juecell.in/api/startups/auth/register.php";
    remove_loading(form);
    form_loading(form);
    if($('#reg_password_confirm').val()!=$('#reg_password').val()){
      form_failed(form,"Password and re-entered passwords must match.");
      e.preventDefault();
      return;
    }
    if($('#reg_password_confirm').val().length<5){
      form_failed(form,"Please enter stronger password");
      e.preventDefault();
      return;
    }
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
              data2 = data;
               if(data.success == 202){
                form_success(form,data.message);
               }
               else {
                form_failed(form,data.message);
               }
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});
$("#recover-password").submit(function(e) {
    var form = $(this);
    var url = "https://startups.juecell.in/api/startups/auth/forgotPasswordChange.php";
    remove_loading(form);
    form_loading(form);
    if($('#reg_password_confirm').val()!=$('#reg_password').val()){
      form_failed(form,"Password and re-entered passwords must match.");
      e.preventDefault();
      return;
    }
    if($('#reg_password_confirm').val().length<5){
      form_failed(form,"Please enter stronger password");
      e.preventDefault();
      return;
    }
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), // serializes the form's elements.
           success: function(data)
           {
               if(data.success == 0){
                form_failed(form,data.message);
               }
               else if(data.success == 1){
                form_success(form,data.message);
                setTimeout(function(){ window.location = "admin.php"; }, 500);
               }
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.
});
function tabShow(f,t){
  if(f==t) return;
  var idOpt = [
    "#login",
    "#register",
    "#forgot"
  ];
  $(idOpt[f]).slideUp(function(){$(idOpt[t]).slideDown();});
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function randomColor(){
  var r = 255-Math.random()*40;
  var g = 255-Math.random()*40;
  var b = 255-Math.random()*40;
  document.getElementsByTagName('BODY')[0].style.background = "rgb("+r+","+g+","+b+")";
}
$('#lg_username').val(atob(getCookie("userHinter")));
randomColor();
