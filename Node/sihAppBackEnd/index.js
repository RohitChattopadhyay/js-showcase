//importing libraries
var express = require("express");
var   bodyParser = require('body-parser');
const helmet = require('helmet')

const path = require("path");
const multer = require("multer");
var busboy = require('connect-busboy');

const mongo = require('./src/scripts/mongo')
const cmd = require('./src/scripts/shellHandler')
const formHandler = require('./src/scripts/formHandler')
// Set up the express app
const app = express();
// // Parse incoming requests data
app.use(helmet())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const fs = require('fs');

//Getting authorization
let auth;


//Express Routers
// SIH
app.post('/confirm',  function(req, res) {
        cmd.ocrConfirmation(req,res)   
    }
);

app.get('/upload', (req,res) => {
    res.sendFile(__dirname + '/src/static/fileUpload.html')
})

app.post('/upload',  function(req, res) {
    formHandler.upload(req,res)
  }
);
//all patients
app.post('/allPatient', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    mongo.getPatients(req,res)
});
// uploadScanned
app.post('/fileSearch', (req, res) => {
    // edit
    res.setHeader('Content-Type', 'application/json');
    mongo.searchKey(req,res)
});
//patient Details
app.post('/patient', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    mongo.getPatient(req,res)
});
//prescription details
app.post('/prescription', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    mongo.getPrescription(req,res)
});
//prescription details
app.post('/downloadRaw', (req, res) => {
    mongo.download(req,res)
});
//sih done

//   //single File Download
// app.get('/api/v1/file/:id', (req, res) => {
//     downloadFile(auth,req,res)
// });

// //File Upload
// app.get('/api/file/upload', (req, res) => {
//     // upload
//     fileHandler.upload(auth,req,res)
// });

// //File Edit
// app.get('/api/file/edit/:id', (req, res) => {
//     // edit
//     fileHandler.edit(auth,req,res)
// });


// //File Rename
// app.get('/api/file/rename/:id/:name', (req, res) => {
//     // edit
//     fileHandler.rename(auth,req,res)
// });

// //File Download
// app.get('/api/file/download/:id', (req, res) => {
//     // download 
//     res.setHeader('Content-Type', 'application/json');
//     fileHandler.download(auth,req,res)
// });

// //Compile
// app.get('/api/compile', (req, res) => {
//     // compile
//     compiler.compile(auth,req,res);
// });
// //Compile
// app.post('/api/compile', (req, res) => {
//     // compile
//     formHandler.compile(auth,req,res)
// });

// app.post('/api/cloudUpload', (req, res) => {
//     // compile
//     formHandler.upload(auth,req,res)
// });

// app.get('/test', (req,res) => {
//     res.sendFile(__dirname + '/src/static/form.html')
// })

const PORT = process.env.PORT || 2413;

//error page handling
app.use((req, res) => {
    res.status(404).send("Sorry can't find that!")
})
  
app.use((err, req, res) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
})

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
});