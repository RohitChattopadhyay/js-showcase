const formidable = require('formidable');
const cmd = require('./shellHandler')
const path = require('path');
const fs = require('fs');
let res,req;
module.exports = {
    //Public methods
    upload: function (request,response) {
      res     =   response;
      req     =   request;
      fileUpload();
    },
    ocrCheck: function (request,response) {
      res     =   response;
      req     =   request;
      ocrConfirmationSender();
    }
}

function fileUpload() {
  var form = new formidable.IncomingForm();
  let date = new Date().getTime()
  form.parse(req, function (err, fields, files) {
      if(err)   throw err
  })
  form.on('fileBegin', function (name, file){
      file.path = './files/pdf/' + date + file.name;
  });
  form.on('file', function (name, file){
    cmd.ocr(date+file.name.split(".")[0],req,res);
  });    
}

function ocrConfirmationForm(){
  
}