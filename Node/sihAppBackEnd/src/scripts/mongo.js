const mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "";//mongoDB connections

//importing libraries
const path = require('path');
const fs = require('fs');
const dir = "./files";
let auth,res,req;
//export module
module.exports = {
    //Public methods
    finalAdd: function(obj){
      upload(obj,"final");
    },
    searchKey: function (request,response) {
        res     =   response;
        req     =   request;
        search();
    },
    getPatients: function(request,response){
      res = response;
      req = request;
      allPatient()
    },
    getPatient: function(request,response){
      res = response;
      req = request;
      patient()
    },
    getPrescription: function(request,response){
      res = response;
      req = request;
      prescriptionDetails()
    },
    download: function(request,response){
      res = response;
      req = request;
      downloadRaw()
    }
};

//functions
function upload(obj,collection){
  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {    
    if (err) throw err;
    var dbo = db.db("prescriptions");
    dbo.collection(collection).insertOne(obj, function(err) {
      if (err) throw err;
      console.log("Added in database");      
      db.close();
    });
  });
}


function search(){
  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
    if (err) throw err;
    var key =  req.body.key!=null?req.body.key:"";
    console.log("Searching for ",key)
    var dbo = db.db("prescriptions");
    dbo.collection("final").find( { $text: { $search: key } } ).toArray(function(err, result) {
      if (err) throw err;
      res.end(JSON.stringify(result))
      db.close();
    });
  });
}

function allPatient(){
  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
    var key =  req.body.key!=null?req.body.key:"";
    if (err) throw err;
    var dbo = db.db("user");
    dbo.collection("patient").find({ $text: {$search: key} }).toArray(function(err, result) {
      if (err) throw err;
      if(result.length==0){
        dbo.collection("patient").find({}).toArray(function(err, result) {
          res.end(JSON.stringify(result)); 
          db.close();
        })
      }
      else{
        res.end(JSON.stringify(result)); 
        db.close();
      }
    });
  });
}


function patient(){
  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
    var key =  req.body.key!=null?req.body.key:"";
    if (err) throw err;
    var dbo = db.db("prescriptions");
    dbo.collection("final").find({"metadata.patID":key}).toArray(function(err, result) {
      if (err) throw err;
      res.end(JSON.stringify(result)); 
      db.close();
    });
  });
}

function prescriptionDetails(){
  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
    var key =  req.body.key!=null?req.body.key:"";
    if (err) throw err;
    var dbo = db.db("prescriptions");
    var oKey = new mongo.ObjectID(key);
    dbo.collection("final").find({"_id":oKey}).toArray(function(err, result) {
      if (err) throw err;
      res.end(JSON.stringify(result)); 
      db.close();
    });
  });
}


function downloadRaw(){
  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
    var key =  (req.body.key!=null&&req.body.key!=undefined)?req.body.key:"5c8b824cf1dd20610bb7ae8c";
    if (err) throw err;
    var dbo = db.db("prescriptions");
    var oKey = new mongo.ObjectID(key);
    dbo.collection("final").find({"_id":oKey}).toArray(function(err, result) {
      if (err) throw err;
      var file = './files/pdf/' + result[0].metadata.fileName + ".pdf";
      res.download(file); // Set disposition and send it.
      console.log("downloaded file")
      db.close();
    });
  });
}
