
//importing libraries
const path = require('path');
const fs = require('fs');
const dir = "./files";
const mongo = require("./mongo")
let auth,res,req;
//export module
module.exports = {
    //Public methods
    nlp: function (fileName,request,response) {
        res     =   response;
        req     =   request;
        nlpFunction(fileName);
    },
    ocr: function (fileName,request,response) {
        res     =   response;
        req     =   request;
        ocrFunction(fileName);
    },
    ocrConfirmation: function (request,response) {
        res     =   response;
        req     =   request;
        ocrConfirm();
    }
};

//Private Methods
//execute
function nlpFunction(fileName,patID,docID){
    console.log('NLP')
    var spawn = require('child_process').spawn;
    var compile = spawn('python', ['grain.py',fileName], {cwd: dir});
    compile.stdout.on('data', function (data) {
        console.warn(data);
    });
    compile.stderr.on('data', function (data) {
        console.warn(String(data));
    });
    compile.on('close', function (data) {
      res.setHeader('Content-Type', 'application/json');
      var nlpJSON = JSON.parse(fs.readFileSync('./files/nlp/'+fileName+'.json', 'utf8'));
      var metadata ={
        fileName : fileName,
        docID: docID,
        patID: patID 
      }
      nlpJSON.metadata = metadata;
      mongo.finalAdd(nlpJSON);
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(
          {
            "result": 200,
            "data" : nlpJSON
          }
        )
      )
    })
}

function ocrFunction(fileName){
  console.log('OCR')
  var spawn = require('child_process').spawn;
	var compile = spawn('./export.sh', [fileName], {cwd: dir});
	compile.stdout.on('data', function (data) {
	  console.warn(data);
	});
	compile.stderr.on('data', function (data) {
	  console.warn(String(data));
	});
	compile.on('close', function (data) {
	console.log("Raw OCR DONE, CHECK")
	var filePath = './files/temp/'+fileName + '.txt';
	  fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
	    if (!err) {
		console.log('received data: ' + data);
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(
		    {
		      "file": fileName,
		      "data": data
		    }
		  )
		)
	    } else {
		console.log(err);
	    }
	  });
	return;
	})    
}

function ocrConfirm(){
  var patientID = req.body.pat;
  var doctorID = req.body.doc;
  var fileName = req.body.file;
  var text = req.body.text;
  fs.writeFile("./files/text/"+fileName+".txt", text, function(err) {
      if(err) {
          return console.log(err);
      } 
      console.log("The file was saved!");
      nlpFunction(fileName,patientID,doctorID);
  }); 
}
