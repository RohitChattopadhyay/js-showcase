const formidable = require('formidable');
const cloud = require('./fileHandler');
const compiler = require('./compileHandler');
const path = require('path');
const fs = require('fs');
let res,auth,req;
module.exports = {
    //Public methods
    upload: function (authorize,request,response) {
        res     =   response;
        req     =   request;
        auth    =   authorize;
        fileUpload();
    },
    compile: function (authorize,request,response) {
        res     =   response;
        req     =   request;
        auth    =   authorize;
        compileFunction();
    }
};

function fileUpload() {
    var form = new formidable.IncomingForm();
    let date = new Date().getTime()
    form.parse(req, function (err, fields, files) {
        if(err)   throw err
    })
    form.on('fileBegin', function (name, file){
        file.path = './temp/uploads/' + date + '/' + file.name;
        ensureDirectoryExistence(file.path)
    });

    form.on('file', function (name, file){
        cloud.upload(file.name,file.type,file.path,auth,req,res);
        deleteFolder(path.dirname(file.path))
    });    
}

function ensureDirectoryExistence(filePath) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
      return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
}
var deleteFolder = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });      
    }
    setTimeout(fs.rmdirSync,2000,path)
  };

  function compileFunction(){
      const body = req.body;
      console.log("Received Form, compiling")
      compiler.compile(body.fileId,body.fileName,'main.c',auth,req,res);
  }