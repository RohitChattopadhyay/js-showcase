//importing libraries
let pidusage = require('pidusage');
const path = require('path');
const fs = require('fs');
const {google} = require('googleapis');

let auth,res,req;
//export module
module.exports = {
    //Public methods
    compile: function (fileIdArr,fileName,fileMain,authorize,request,response) {
        res     =   response;
        req     =   request;
        auth    =   authorize
        download(fileIdArr,fileName,fileMain);
    }
};

//Private Methods
//function to send download
function download(fileId,fileName,fileMain){
    const drive = google.drive({version: 'v3', auth});
    let date = new Date().getTime()
    const dirname = fileId[0]+date;
    for(let i=0;i<fileId.length;i++){
        drive.files.get({
            fields: 'name',
            fileId: fileId[i]
          }, (err, res) => {
            if (err) {
                console.log(err)
                return 0;
            }
            if(i===fileId.length-1) getFile(fileId[i],res.data.name,dirname + fileName[i],'');
            else getFile(fileId[i],res.data.name,dirname + fileName[i],fileMain);
        });
    }
}

//get individual files
function getFile(fileId,name,dirname,execute) {
    dirname = "./temp/compilation/" + dirname + "/";
    const drive = google.drive({version: 'v3', auth});
    ensureDirectoryExistence(dirname + name)
    var dest = fs.createWriteStream(dirname + name);
    drive.files.get(
        {
            fileId: fileId, alt: 'media' , 
            fields: 'name'
        },
        {
            responseType: 'stream'
        },
        function(err, res){
            res.data
            .on('end', () => {
                console.log('Download Success');
                if(execute.length>1) {
                    setTimeout(executeC,2000,execute,dirname,'10')
                }
            })
            .on('error', err => {
               console.log('Error', err);
            })
            .pipe(dest);
         }    
    );
}

//execute
function executeC(fileName,dir,stdIn = ''){
    console.log('compiling')
    var stream   = require('stream');
    
    var outLog = fs.createWriteStream(dir + 'output.txt');
    var errLog = fs.createWriteStream(dir + 'error.txt');
    let errMsg = ''
    var spawn = require('child_process').spawn;
    var compile = spawn('g++', [fileName,'-lm'], {cwd: dir});
    compile.stdout.on('data', function (data) {
        errLog.write('stdout: ' + data);
    });
    compile.stderr.on('data', function (data) {
        errLog.write(String(data));
    });
    compile.on('close', function (data) {
        if (data === 0) {
            var run = spawn('./a.out', [], {cwd: dir});
            var stdinStream = new stream.Readable();
            stdinStream.push(stdIn);  // Add data to the internal queue for users of the stream to consume
            stdinStream.push(null);   // Signals the end of the stream (EOF)
            stdinStream.pipe(run.stdin);

            var memout = setInterval(() => {
                pidusage(run.pid, function (err, stats) {
                    var flag = 0;
                    if(stats.memory> 6*1024){
                        errMsg += ' MEMORY ';
                        flag++;
                    }
                    if(stats.elapsed> 10000){
                        errMsg += ' RUNTIME ';
                        flag++;
                    }
                    if(flag>0) {
                        try {
                            run.kill();
                        } catch (e) {
                            console.log('Cannot kill process',e);
                        }
                    }
                });
            }, 1000);

            run.stdout.on('data', function (output) {
                outLog.write(String(output));
            });
            run.stderr.on('data', function (output) {
                errLog.write(String(output));
            });
            run.on('close', function (output) {
                outLog.write('\nProgram returned ' + (errMsg.length>3?errMsg:output));
                console.log('compiled')
                outLog.end()
                errLog.end()
                generateResponse(dir);
                clearInterval(memout);
            })
        }
    })
}

// Create 
function ensureDirectoryExistence(filePath) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
      return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
  }

//createResponse
function generateResponse(dir){
    console.log('Generating Response');
    fs.readFile(dir + 'output.txt','utf-8', function(e,d) {
        if (e) {
        console.log(e);
            res.send(500, 'Something went wrong');
        }
        else {
            res.send(d);
        }
        deleteFolderRecursive(dir)
    })
}
var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file,index){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });      
    }
    setTimeout(fs.rmdirSync,2000,path)
  };