//importing libraries
const fs = require('fs');
const {google} = require('googleapis');

let auth,res,req;
//export module
module.exports = {
    upload: function (name,mime,dir,authorize,request,response) {
        res     =   response;
        req     =   request;
        auth    =   authorize
        uploadFile(name,mime,dir);
    },
    edit: function (authorize,request,response) {
      res     =   response;
      req     =   request;
      auth    =   authorize
        editFile();
    },
    rename: function (authorize,request,response){
      res     =   response;
      req     =   request;
      auth    =   authorize
        renameFile();
    },
    download: function(authorize,request,response) {
      res     =   response;
      req     =   request;
      auth    =   authorize
      singleFileDownload();
    }
};

function uploadFile(fileName,fileMime,fileDir) {
    var fileMetadata = {
        'name': fileName
      };
      var media = {
        mimeType: fileMime,
        body: fs.createReadStream(fileDir)
      };
    const drive = google.drive({version: 'v3', auth});
    drive.files.create({
        resource: fileMetadata,
        media: media,
        fields: 'id,name,parents'
      }, function (err, file) {
        if (err) {
          // Handle error
          console.error(err);
        } else {
          console.log(file.data);
        }
    });
}

function editFile(){
  var fileMetadata = {
      'name': 'photoTest.jpg'
    };
    var media = {
      mimeType: 'text/plain',
      body: fs.createReadStream('./files/update.txt')
    };
  const drive = google.drive({version: 'v3', auth});
  drive.files.update({
      fileId: '1d7zzDxpHeOrLGIEJr34myn50ybaTu2wc',
      resource: {},
      media: media
    }, function (err, file) {
      if (err) {
        // Handle error        
        res.json(
          {
            status: err.response.status
          }
        )
      } else {        
        res.json(
          {
            status: 200            
          }
        )
      }
  });
}

function renameFile(){
  var fileMetadata = {
      'name': req.params.name
    };
  const drive = google.drive({version: 'v3', auth});
  drive.files.update({
      fileId: req.params.id,
      keepRevisionForever: true,
      resource: fileMetadata
    }, function (err, file) {
      if (err) {
        // Handle error
        res.json(
          {
            status: err.response.status
          }
        )
      } else {
        console.log(file.data);
        res.json(
          {
            status : '200'
          }
        )
      }
  });
}

function singleFileDownload(){
  const drive = google.drive({version: 'v3', auth});
  const fileID = req.params.id
  let metaRes,mediaRes,flag = 1;
  drive.files.get({
      fileId: fileID
    }, function (err, file) {
      if (err) {
        // Handle error
        res.json(
          {
            status: err.response.status
          }
        )
      } else {        
        metaRes = file.data;
        drive.files.get({
            fileId: fileID,
            alt : 'media',
            fields: 'name'
          }, function (err, file) {
            if (err) {
              // Handle error              
              res.json(
                {
                  status: err.response.status
                }
              )
            } else {        
              mediaRes = file.data;
              res.json(
                  {
                  status : 200,
                  name : metaRes.name,
                  mime : metaRes.mimeType,
                  content : mediaRes 
                  }
                
              );
            }
        });
      }
  });
}