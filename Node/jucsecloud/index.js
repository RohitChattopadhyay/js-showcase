//importing libraries
var express = require("express");
var bodyParser = require('body-parser');
const helmet = require('helmet')

const compiler = require('./src/scripts/compileHandler')
const fileHandler = require('./src/scripts/fileHandler')
const formHandler = require('./src/scripts/formHandler')
// Set up the express app
const app = express();
// Parse incoming requests data
app.use(helmet())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const fs = require('fs');
const {google} = require('googleapis');

//Getting authorization
let auth;
fs.readFile('./src/authorize/credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Drive API.
    const credentials = JSON.parse(content)
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile('./src/authorize/token.json', (err, token) => {
        oAuth2Client.setCredentials(JSON.parse(token));
        auth = oAuth2Client;
        console.log("auth token generated")
    });
});

//Express Routers

//single File Download
app.get('/api/v1/file/:id', (req, res) => {
    downloadFile(auth,req,res)
});

//File Upload
app.get('/api/file/upload', (req, res) => {
    // upload
    fileHandler.upload(auth,req,res)
});

//File Edit
app.get('/api/file/edit/:id', (req, res) => {
    // edit
    fileHandler.edit(auth,req,res)
});


//File Rename
app.get('/api/file/rename/:id/:name', (req, res) => {
    // edit
    fileHandler.rename(auth,req,res)
});

//File Download
app.get('/api/file/download/:id', (req, res) => {
    // download 
    res.setHeader('Content-Type', 'application/json');
    fileHandler.download(auth,req,res)
});

//Compile
app.get('/api/compile', (req, res) => {
    // compile
    compiler.compile(auth,req,res);
});
//Compile
app.post('/api/compile', (req, res) => {
    // compile
    formHandler.compile(auth,req,res)
});

app.post('/api/cloudUpload', (req, res) => {
    // compile
    formHandler.upload(auth,req,res)
});

app.get('/test', (req,res) => {
    res.sendFile(__dirname + '/src/static/form.html')
})

const PORT = process.env.PORT || 5000;



//error page handling
app.use((req, res) => {
    res.status(404).send("Sorry can't find that!")
})
  
app.use((err, req, res) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
})

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
});
