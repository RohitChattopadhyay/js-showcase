The folder contains 2 works
# 1.jucsecloud
A file management system based on Google Cloud(Drive) API for my department. Files related to authorization has not been uploaded.
- NodeJS
- ExpressJS
- MySQL

# 2.sihAppBackEnd
This is the Back End of an application developed in Grand Finale of Smart India Hackathon, a 36 hour Hackathon conducted by Government of India.
- NodeJS
- ExpressJS
- MongoDB
